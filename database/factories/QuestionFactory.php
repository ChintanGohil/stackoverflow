<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

// $factory->define(Question::class, function (Faker $faker) {
//     return [
//         'title' => rtrim($faker->sentence(rand(5, 10))),'.',
//         'body' => $faker->paragraphs(rand(3, 7), true),
//         'views_count' => rand(0, 10),
//         'answers_count' => rand(0, 10),
//         'votes_count' => rand(-10, 10)
//     ];
// });

$factory->define(Question::class, function (Faker $faker) {
    return [
        'title'=> rtrim($faker->sentence(rand(5, 10)), '.'),
        'body'=> $faker->paragraph(rand(3, 7), true),
        'views_count'=> rand(0, 10),//eloquent Event Handling retrived, creating, creaded, updating, update, saved, deleted, deleting, restoring, restored
        'votes_count'=> rand(-10, 10)
    ];
    //to use eloquent event we need to register the event in model
});
